import request from './request';

export default class ApiService {
    static get(url) {
        return request({
            url,
            method: 'GET'
        });
    };

    static post(url, data) {
        return request({
            url: url,
            method: 'POST',
            data
        });
    };
}