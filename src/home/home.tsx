import * as React from 'react';
import ApiService from '../utils/api-service';
import './home.scss';

export interface ITemplate {
    title: string;
    cost: string;
    id: string;
    description: string;
    thumbnail: string;
    image: string;
};

interface IHomeState {
    thumbnailsIndex: number,
    thumbnailsPerPage: number,
    totalThumbnails: number,
    thumbnails: ITemplate[],
    templates: ITemplate[],
    selectedImage: ITemplate | null
}

class Home extends React.Component<any, IHomeState> {

    private images = require.context('../assets/images/large', true);


    public constructor(props: any) {
        super(props);
        this.state = {
            thumbnailsIndex: 0,
            thumbnailsPerPage: 4,
            totalThumbnails: 0,
            thumbnails: [],
            templates: [],
            selectedImage: null
        };
    }

    loadPreviousThumbnails = () => {
        this.setState({
            ...this.state,
            thumbnailsIndex:  this.state.thumbnailsIndex - this.state.thumbnailsPerPage
        }, () => {
            this.loadThumbnails();
        });
    }

    loadImage = (imageName: string) => {
        if (!imageName) return imageName;
        return this.images(`./${imageName}`).default
    }

    loadNextThumbnails = () => {
        this.setState({
            ...this.state,
            thumbnailsIndex: this.state.thumbnailsPerPage + this.state.thumbnailsIndex
        }, () => {
            this.loadThumbnails();
        });
    }

    setSelectedImage = (template: ITemplate) => {
        this.setState({
            ...this.state,
            selectedImage: template
        });
    }

    loadThumbnails = () => {
        const updatedThumbs = this.state.templates.slice(this.state.thumbnailsIndex, this.state.thumbnailsIndex + this.state.thumbnailsPerPage);
        this.setState({
            ...this.state,
            thumbnails: updatedThumbs
        });
    }

    componentDidMount() {
        ApiService.get('/templates').then((res) => {
            this.setState({
                ...this.state,
                templates: res,
                totalThumbnails: res.length,
                selectedImage: res[0]
            }, () => {
                this.loadThumbnails();
            });
        });
    }

 public render() {
        return <div id="container">
            <header>
                Code Development Project
            </header>

            <div id="main" role="main" >
                {/* Large image container starts */}
                <div id="large">

                    <div className="group">
                        {
                            this.state.selectedImage ? <>
                                <img src={this.loadImage(this.state.selectedImage.image)} alt="" width="430" height="360" />
                                <div className="details">
                                    <p><strong>Title</strong> {this.state.selectedImage.title}</p>
                                    <p><strong>Description</strong> {this.state.selectedImage.description}</p>
                                    <p><strong>Cost</strong> ${this.state.selectedImage.cost}</p>
                                    <p><strong>ID #</strong> {this.state.selectedImage.id}</p>
                                    <p><strong>Thumbnail File</strong> {this.state.selectedImage.thumbnail}</p>
                                    <p><strong>Large Image File</strong> {this.state.selectedImage.image}</p>
                                </div>
                            </> : null
                        }
                    </div>
                </div>
                {/* Large image container ends */}

                {/* Thumbnails container starts */}
                <div className="thumbnails">
                    <div className="group">
                        {this.state.thumbnails.map((template: ITemplate) => {
                            return <React.Fragment key={template.id}>
                                <a onClick={() => this.setSelectedImage(template)} title={template.title} className={this.state.selectedImage && this.state.selectedImage.id === template.id ? 'active': ''}>
                                    <img src={this.loadImage(template.image)} alt={template.thumbnail} width="145" height="121" />
                                    <span>{template.id}</span>
                                </a>
                                <button disabled={this.state.thumbnailsIndex === 0} className={this.state.thumbnailsIndex === 0 ? 'previous disabled' : 'previous'} title="Previous" onClick={this.loadPreviousThumbnails}>Previous</button>
                                <button disabled={(this.state.thumbnailsIndex + this.state.thumbnailsPerPage) >= this.state.totalThumbnails} title="Next" onClick={this.loadNextThumbnails} className={(this.state.thumbnailsIndex + this.state.thumbnailsPerPage) >= this.state.totalThumbnails ? 'next disabled' : 'next'}>Next</button>
                            </React.Fragment>
                        })
                        }
                    </div>
                </div>
                {/* Thumbnails container ends */}
            </div>

            <footer>
                <a href="#">Download PDF Instructions</a>
            </footer>
        </div>
    }
}


export default Home;