const importAll = (r: any) => {
    return r.keys().map(r);
  }
  
const images = importAll(require.context('./', false, /\.(png|jpe?g|svg)$/));
let imagesMap: Record<string,string> = {};

images.forEach((image: any) => {
    const imagePathParts = image.default.split('/');
    imagesMap[imagePathParts[imagePathParts.length-1]] = image;
});

export default imagesMap;