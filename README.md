# Project Title : Website Template Viewer

The template viewer is designed in a filmstrip view which displays thumbnail images with one large image.

## Clone the project repository with VS Code

1. From the repository, click clone button.
2. Select Clone in VS Code option in the Clone this repository window.
3. Click open Visual Studio Code.
4. In VS Code, select Clone a new copy from the menu options.
5. Select the local folder to clone the repository.
6. Select the select repository location button.
7. Repository is cloned with VS Code. Open the repository from VS Code.

## Clone the project repository with command line

1. From the repository, click clone button.
2. Copy the git clone command for HTTPS.
3. Open the terminal and go the local directory where you want to clone this repository.
4. Paste the git clone command and click enter.
5. Clone is done and all files are available in the local directory. Open the folder using VS Code.


## Available Scripts - To run the project

### `npm install`

This will install the node_modules.

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

# Website Design

Sliding window screen will be displayed with one large image, thumbnail images, previous and next links to slide and view the available templates.

When user clicks on the thumbnail, respective thumbnail image will be displayed as large image along with its metadata.

Previous link will be disabled and next link will be enabled on the initial page. 

Vice vera, Previous link will be enabled and next link will be disabled on the last page.


## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
